<?xml version='1.0' encoding='UTF-8'?>
<!-- Schema file written by PDE -->
<schema targetNamespace="de.tud.cs.st.bat.findrealbugs.plugin" xmlns="http://www.w3.org/2001/XMLSchema">
<annotation>
      <appinfo>
         <meta.schema plugin="de.tud.cs.st.bat.findrealbugs.plugin" id="de.tud.cs.st.bat.findrealbugs.plugin.analysis" name="FindREALBugs analysis"/>
      </appinfo>
      <documentation>
         This extension point allows the implementation of a FindREALBugs analysis in an Eclipse plugin.
      </documentation>
   </annotation>

   <element name="extension">
      <annotation>
         <appinfo>
            <meta.element />
         </appinfo>
      </annotation>
      <complexType>
         <choice minOccurs="1" maxOccurs="unbounded">
            <element ref="analysis"/>
         </choice>
         <attribute name="point" type="string" use="required">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="id" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
               <appinfo>
                  <meta.attribute translatable="true"/>
               </appinfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="analysis">
      <annotation>
         <documentation>
            Used to implement a FindREALBugs analysis in an Eclipse plugin.
         </documentation>
      </annotation>
      <complexType>
         <attribute name="name" type="string" use="required">
            <annotation>
               <documentation>
                  This is a unique name for the analysis. This name will be used throughout the entire FindREALBugs Eclipse plugin to identify the analysis and as title.
               </documentation>
            </annotation>
         </attribute>
         <attribute name="userDocURL" type="string">
            <annotation>
               <documentation>
                  If the user documentation is not at the standard FindREALBugs website, a URL to the user documentation can be expicitely provided. If no URL is specified, the FindREALBugs plugin will assume, that the documentation is on the default website!
               </documentation>
            </annotation>
         </attribute>
         <attribute name="class" type="string" use="required">
            <annotation>
               <documentation>
                  This has to be a MultipleResultsAnalysis. For further information look at the FindREALBugs scaladoc.
               </documentation>
               <appinfo>
                  <meta.attribute kind="java" basedOn="de.tud.cs.st.bat.resolved.analyses.MultipleResultsAnalysis:"/>
               </appinfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <annotation>
      <appinfo>
         <meta.section type="since"/>
      </appinfo>
      <documentation>
         Version 0.1
      </documentation>
   </annotation>

   <annotation>
      <appinfo>
         <meta.section type="examples"/>
      </appinfo>
      <documentation>
         &lt;p&gt;
			plugin.xml:&lt;br /&gt;
			&lt;br /&gt;
			...&lt;br /&gt;
			&amp;nbsp;&amp;lt;extension&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; point=&amp;quot;de.tud.cs.st.bat.findrealbugs.plugin.analysis&amp;quot;&amp;gt;&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;lt;analysis&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; name=&amp;quot;TestAnalysis&amp;quot;&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; descriptionUrl=&amp;quot;http://www.wikipedia.org&amp;quot;&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; class=&amp;quot;sampleanalysisplugin.TestAnalysis&amp;quot;&amp;gt;&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;lt;/analysis&amp;gt;&lt;br /&gt;
			&amp;nbsp;&amp;lt;/extension&amp;gt;&lt;br /&gt;
			...&lt;br /&gt;
			&lt;br /&gt;
			&lt;br /&gt;
			TestAnalysis.scala:&lt;br /&gt;
			&lt;br /&gt;
			import de.tud.cs.st.bat.resolved.analyses.Project&lt;br /&gt;
			import de.tud.cs.st.bat.resolved.analyses.MultipleResultsAnalysis&lt;br /&gt;
			import de.tud.cs.st.bat.resolved.analyses.FieldBasedReport&lt;br /&gt;
			import de.tud.cs.st.bat.resolved.Field&lt;br /&gt;
			import de.tud.cs.st.bat.resolved.ObjectType&lt;br /&gt;
			import de.tud.cs.st.bat.resolved.analyses.Severity&lt;br /&gt;
			import java.net.URL&lt;br /&gt;
			import de.tud.cs.st.bat.resolved.analyses.SourceLocationBasedReport&lt;br /&gt;
			&lt;br /&gt;
			package sampleanalysisplugin {&lt;br /&gt;
			&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp; class TestAnalysis&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; extends MultipleResultsAnalysis[URL, SourceLocationBasedReport[URL]] {&lt;br /&gt;
			&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; def description: String = &amp;quot;Reports fields that are 3 characters long or shorter.&amp;quot;&lt;br /&gt;
			&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; def analyze(&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; project: Project[URL],&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; parameters: Seq[String] = List.empty): Iterable[FieldBasedReport[URL]] = {&lt;br /&gt;
			&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; val shortFields = for {&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; classFile &amp;larr; project.classFiles if !classFile.isInterfaceDeclaration&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; field &amp;larr; classFile.fields&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; if field.name.size &amp;lt;= 3&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; } yield {&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; (classFile, field)&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; }&lt;br /&gt;
			&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; for ((classFile, field) &amp;larr; shortFields) yield {&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; FieldBasedReport(&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; project.source(classFile.thisType),&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Severity.Info,&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; classFile.thisType,&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; field,&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;quot;has a very short name&amp;quot;)&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; }&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; }&lt;br /&gt;
			&amp;nbsp;&amp;nbsp;&amp;nbsp; }&lt;br /&gt;
			}&lt;/p&gt;
      </documentation>
   </annotation>

   <annotation>
      <appinfo>
         <meta.section type="apiinfo"/>
      </appinfo>
      <documentation>
         For api information see: https://bitbucket.org/delors/bat/
      </documentation>
   </annotation>



</schema>
