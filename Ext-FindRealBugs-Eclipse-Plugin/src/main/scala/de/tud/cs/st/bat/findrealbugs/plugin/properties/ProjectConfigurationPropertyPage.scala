/* License (BSD Style License):
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package de.tud.cs.st
package bat
package findrealbugs
package plugin
package properties

import extension.ExtensionAnalyses
import org.eclipse.core.resources._
import org.eclipse.core.runtime._
import org.eclipse.swt.events.SelectionListener
import org.eclipse.swt.events.SelectionEvent
import org.eclipse.swt.layout._
import org.eclipse.swt.widgets._
import org.eclipse.swt.SWT
import org.eclipse.jdt.core.IJavaProject
import org.eclipse.jface.dialogs.MessageDialog
import org.eclipse.ui.dialogs.PropertyPage
import java.util.Properties
import java.io._

/**
 * Implements the FindRealBugs (Java-)project property page. It displays a list of all
 * analyses together with check boxes indicating if they are enabled for the project. On
 * top of it are two buttons for importing and exporting the currently displayed
 * configuration.
 *
 * Additionally, a "Restore defaults" and a "Apply" Button are provided. The state of
 * the list is stored persistently inside the project preferences when the "Apply" or "OK"
 * button is clicked.
 *
 * @author Florian Brandherm
 */
class ProjectConfigurationPropertyPage extends PropertyPage {
    // The table that contains the analyses along with checkboxes
    private var disabledAnalysesTable: Table = null;

    /**
     * Creates the contents of this page as child element of parent.
     * This is called by the superclass!
     *
     * @param parent `Composite` which is intended to be parent to the created element.
     * @return A `Composite` object which contains all swt-gui-elements of the
     * properties page.
     */
    protected def createContents(parent: Composite): Control = {
        val composite = new Composite(parent, SWT.NONE)

        // Create a new grid layout with 1 column
        val layout = new GridLayout()
        layout.numColumns = 1
        composite.setLayout(layout)

        val data = new GridData()
        data.verticalAlignment = GridData.FILL
        data.horizontalAlignment = GridData.FILL

        composite.setLayoutData(data)

        // Create GUI
        createImportExportButtons(composite)
        createLabel(composite)
        createTable(composite)

        // Fill it with data
        loadData()

        composite
    }

    /**
     * Adds two buttons for importing and exporting the settings to the GUI.
     *
     * @param parent `Composite` which is intended to be parent to the created element.
     */
    private def createImportExportButtons(parent: Composite) {
        // Composite that will hold the two buttons horizontally
        val composite = new Composite(parent, SWT.NONE)

        val layout = new GridLayout()
        layout.numColumns = 2
        composite.setLayout(layout)

        val data = new GridData()
        data.verticalAlignment = GridData.FILL
        data.horizontalAlignment = GridData.FILL
        data.grabExcessHorizontalSpace = true
        data.grabExcessVerticalSpace = true

        composite.setLayoutData(data)

        // Create import button
        val importButton = new Button(composite, SWT.PUSH)
        importButton.setText("Import settings")

        importButton.addSelectionListener(new SelectionListener() {
            /**
             * Unused, but must be implemented.
             */
            def widgetDefaultSelected(e: SelectionEvent) {
            }

            /**
             * Import button click event handler: Show a file chooser dialog and, if the
             * user chooses a valid file, load the configuration from the chosen file into
             * the GUI.
             *
             * @param e unused
             */
            def widgetSelected(e: SelectionEvent) {
                // Import
                val fileDialog = new FileDialog(importButton.getShell(), SWT.OPEN)
                fileDialog.setText("Import")

                val path = fileDialog.open()
                if (path != null) {
                    try {
                        // Import settings into the table
                        val diabledAnaylsesNames = ConfigurationFile.
                            getDisabledAnalysesNamesFromFile(path)
                        fillTableEntries(diabledAnaylsesNames)
                    } catch {
                        case ex: IOException ⇒
                            // Show error to the user
                            MessageDialog.openInformation(
                                getShell(),
                                "FindREALBugs",
                                "Could not import settings.")
                    }
                }
            }
        })

        // Create export button
        val exportButton = new Button(composite, SWT.PUSH)
        exportButton.setText("Export settings")

        exportButton.addSelectionListener(new SelectionListener() {
            /**
             * Unused, but must be implemented.
             */
            def widgetDefaultSelected(e: SelectionEvent) {
            }

            /**
             * Export button click event handler: Show a file chooser dialog and, if the
             * user chooses a valid file, save the configuration from the GUI table into
             * the chosen file.
             *
             * @param e unused
             */
            def widgetSelected(e: SelectionEvent) {
                // Export
                val fileDialog = new FileDialog(exportButton.getShell(), SWT.SAVE)
                fileDialog.setText("Save")

                fileDialog.setFileName("FindREALBugs.properties")
                val path = fileDialog.open()
                if (path != null) {
                    try {
                        ConfigurationFile.saveDisabledAnalysesToFile(path,
                            getDisabledAnalysesFromGUI)
                    } catch {
                        case ex: IOException ⇒
                            // Show error to the user
                            MessageDialog.openInformation(
                                getShell(),
                                "FindREALBugs",
                                "Could not export settings.")
                    }
                }
            }
        })
    }

    /**
     * Creates a label element which sits on top of the list, saying "Enabled analyses:".
     *
     * @param parent element that is intended to be parent to the created label
     */
    private def createLabel(parent: Composite) {
        val label = new Label(parent, SWT.NONE)
        label.setText("Enabled analyses:")
    }

    /**
     * Initializes field table that contains all analyses names with check boxes.
     * It will contain the persistent analysis activation state.
     *
     * @param parent element that is intended to be parent to the created table
     */
    private def createTable(parent: Composite) {
        disabledAnalysesTable =
            new Table(parent, SWT.CHECK | SWT.BORDER | SWT.V_SCROLL | SWT.SINGLE)

        val gridData = new GridData();
        gridData.verticalAlignment = GridData.BEGINNING;
        gridData.verticalSpan = 1;
        gridData.horizontalAlignment = GridData.FILL;
        gridData.grabExcessHorizontalSpace = true;

        disabledAnalysesTable.setLayoutData(gridData)
    }

    /**
     * Loads the persistently stored disabled analyses list and fills the GUI table with
     * this configuration.
     */
    private def loadData() {
        fillTableEntries(ProjectConfigurationPropertyPage.
            getDisabledAnalysesNames(getProject))
    }

    /**
     * Fills a given table with all analyses names and initializers the check boxes
     * with the information about whether certain analyses are enabled or disabled.
     *
     * @param disabledAnalyses `Iterable` containing the names of all analyses that should
     * be marked as disabled in the GUI table.
     */
    private def fillTableEntries(disabledAnalyses: Iterable[String]) {
        // Remove existing entries, if any
        for (item ← disabledAnalysesTable.getItems()) {
            item.dispose()
        }

        val allAnalysisNames = (FindRealBugs.builtInAnalyses.keys.toList :::
            ExtensionAnalyses.analyses.keys.toList).sorted

        for (analysisName ← allAnalysisNames) {
            val listItem = new TableItem(disabledAnalysesTable, SWT.NONE)
            listItem.setText(analysisName)
            listItem.setChecked(!disabledAnalyses.exists(_ == analysisName))
        }
    }

    /**
     * This method is called by the superclass, if the user clicks the button
     * "Restore Defaults". It will set all analyses to enabled in the GUI table.
     */
    override protected def performDefaults() {
        super.performDefaults()
        for (item ← disabledAnalysesTable.getItems()) {
            item.setChecked(true)
        }
    }

    /**
     * This method is called by the superclass, if the user clicks the button "Apply".
     * It stores the list of disabled analyses persistently.
     *
     * Note: these settings are saved inside the .settings folder of the project and not
     * as properties!
     *
     * @return true, if successful, false, if unsuccessful.
     */
    override def performOk(): Boolean = {
        // Collect disabled analyses
        val ignoredList = getDisabledAnalysesFromGUI

        ProjectConfigurationPropertyPage.
            saveDisabledAnalysisNames(getProject(), ignoredList)

        return true;
    }

    /**
     * Retrieves the analysis names that are currently marked as disabled in the table.
     *
     * @return `Array` of analysis names that are disabled according to the GUI.
     */
    private def getDisabledAnalysesFromGUI(): Array[String] = {
        for {
            item ← disabledAnalysesTable.getItems()
            if !item.getChecked()
        } yield {
            item.getText
        }
    }

    /**
     * Returns the java project that this property page is displayed for.
     *
     * @return The java project of this property page.
     */
    private def getProject(): IJavaProject = {
        getElement().getAdapter(classOf[IJavaProject]).asInstanceOf[IJavaProject]
    }
}

/**
 * Provides methods for loading and saving a list of the names of all disabled analyses
 * for java projects as a project preference.
 *
 * @author Florian Brandherm
 */
object ProjectConfigurationPropertyPage {
    /**
     * QualifiedName of the property that stores a comma-separated list of disabled
     * names.
     */
    private val disabledAnalysesPreferenceName: String = "disabled_analyses"

    /**
     * This is the name of the node these settings are saved in.
     */
    private val nodeName = "de.tud.cs.st.bat.findrealbugs.plugin"

    /**
     * Retrieves an `Iterable` of all disabled analyses names from the preferences of a
     * given Java project.
     *
     * @param project The Java project for which the disabled analyses are requested.
     * @return `Iterable` containing the names of all disabled analysis names.
     */
    def getDisabledAnalysesNames(project: IJavaProject): Iterable[String] = {
        // Acquire the project specific preferences node
        val projectScope = new ProjectScope(project.getProject())
        val projectNode = projectScope.getNode(nodeName)
        if (projectNode != null) {
            // Get the list of ignored analyses (default is "")
            projectNode.get(disabledAnalysesPreferenceName, "").split(",")
        } else {
            Nil
        }
    }

    /**
     * Saves the disabled analyses for a given java project persistently as a preference.
     *
     * @param project Project that the disabled analyses are stored for.
     * @param disabledAnalyses Iterable of the names of all disabled analyses.
     */
    def saveDisabledAnalysisNames(
        project: IJavaProject,
        disabledAnalyses: Iterable[String]) {

        // Acquire the project specific preferences node
        val projectScope = new ProjectScope(project.getProject())
        val projectNode = projectScope.getNode(nodeName)

        // Save the ignored analyses as comma-newline-separated list
        if (projectNode != null) {
            projectNode.put(disabledAnalysesPreferenceName, disabledAnalyses.mkString(","))
        }

        // Save the changes to the node
        projectNode.flush()
    }
}
