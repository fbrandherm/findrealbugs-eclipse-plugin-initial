/* License (BSD Style License):
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.tud.cs.st
package bat
package findrealbugs
package plugin.views

import resolved.analyses._
import java.net.URL
import org.eclipse.jdt.core._
import org.eclipse.jdt.core.dom._
import org.eclipse.core.resources.ResourcesPlugin
import plugin.ResourceLocationUtility

/**
 * Data structure for the FindREALBugs view.
 * It contains all needed data for displaying and opening the related file.
 *
 * @param className name of the reported class (null, if unknown)
 * @param severity severity of the bug
 * @param label label that should be displayed for the report
 * @author Florian Brandherm
 */
class ReportData(
        val className: String,
        val severity: Severity,
        val label: String) {

    /**
     * Returns the line number of the report if known.
     * Override this method if you can determine the line number.
     * If this method is not overridden, it will always return None.
     *
     * @return Some(line number) if the line number can be determined. None otherwise.
     */
    def linenumber: Option[Int] = None

    /**
     * Checks for structural equality.
     *
     * @param obj Object that should be checked for equality.
     * @return true, if the two objects have equal values, false otherwise
     */
    override def equals(obj: Any): Boolean = {
        if (obj.isInstanceOf[ReportData]) {
            val objReportData = obj.asInstanceOf[ReportData]
            objReportData.className == className &&
                objReportData.severity == severity &&
                objReportData.label == label
        } else {
            false
        }
    }
}

/**
 * Companion object with factory methods for creating a `ReportData` object from various
 * BAT `SourceLocationBasedReports`.
 *
 * @author Florian Brandherm
 */
object ReportData {

    import ResourceLocationUtility._

    /**
     * Factory method that creates a `ReportData` object from any
     * `SourceLocationBasedReport`.
     *
     * @param report Report from which the ReportData object should be created.
     * @return ReportData object that was created from the data insinde report.
     */
    def apply(
        report: SourceLocationBasedReport[URL],
        project: IJavaProject): ReportData = {

        report match {
            case classBasedReport: ClassBasedReport[URL] ⇒
                createFromClassBasedReport(classBasedReport, project)
            case methodBasedReport: MethodBasedReport[URL] ⇒
                createFromMethodBasedReport(methodBasedReport, project)
            case fieldBasedReport: FieldBasedReport[URL] ⇒
                createFromFieldBasedReport(fieldBasedReport, project)
            case lineAndColumnBasedReport: LineAndColumnBasedReport[URL] ⇒
                createFromLineAndColumnBasedReport(lineAndColumnBasedReport)
            case _ ⇒ null
        }

    }

    /**
     * Creates a `ReportData` object from a `ClassBasedReport`
     *
     * @param report Report from which the ReportData object should be created.
     * @param project Java Project that this report belongs to. This will be used
     * to determine the line number of the reported class.
     * @return ReportData object that was created from the data inside report.
     */
    private def createFromClassBasedReport(
        report: ClassBasedReport[_],
        project: IJavaProject): ReportData = {

        import report._
        val className = classType.toJava

        new ReportData(className, severity, "class "+className+": "+message) {
            override def linenumber = {
                if (project == null) {
                    None
                } else {
                    getClassLineNumber(className, project)
                }
            }
        }
    }

    /**
     * Creates a `ReportData` object from a `MethodBasedReport`.
     *
     * @param report Report from which the ReportData object should be created.
     * @param project Java Project that this report belongs to. This will be used
     * to determine the line number of the reported method.
     * @return ReportData object that was created from the data inside report.
     */
    private def createFromMethodBasedReport(
        report: MethodBasedReport[_],
        project: IJavaProject): ReportData = {

        import report._
        val className = declaringClass.toJava

        val params =
            report.methodDescriptor.parameterTypes.map(_.toBinaryJavaName).toArray

        new ReportData(className, severity, "in class "+className+": method "+
            methodName+": "+message) {
            override def linenumber = {
                if (project == null) {
                    None
                } else {
                    getMethodLineNumber(className, methodName, params, project)
                }
            }
        }
    }

    /**
     * Creates a `ReportData` object from a `FieldBasedReport`.
     *
     * @param report Report from which the ReportData object should be created.
     * @param project Java Project that this report belongs to. This will be used
     * to determine the line number of the reported field.
     * @return ReportData object that was created from the data inside report.
     */
    private def createFromFieldBasedReport(
        report: FieldBasedReport[_],
        project: IJavaProject): ReportData = {

        import report._
        val className = declaringClass.toJava

        new ReportData(className, severity, "in class "+className+": field "+fieldName+
            ": "+message) {
            override def linenumber = {
                if (project == null) {
                    None
                } else {
                    getFieldLineNumber(className, fieldName, project)
                }
            }
        }
    }

    /**
     * Creates a `ReportData` object from a `LineAndColumnBasedReport`.
     *
     * @param report Report from which the ReportData object should be created.
     * @return ReportData object that was created from the data inside report.
     */
    private def createFromLineAndColumnBasedReport(
        report: LineAndColumnBasedReport[_]): ReportData = {

        import report._
        val className = declaringClass.toJava

        new ReportData(className, severity, "in class "+className +
            line.map(" at line "+_+":").getOrElse(":") +
            column.map(_+": ").getOrElse(" ") + message) {
            override def linenumber = line
        }
    }
}
