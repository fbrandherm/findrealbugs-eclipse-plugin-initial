/* License (BSD Style License):
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.tud.cs.st
package bat
package findrealbugs
package plugin
package test
package views

import plugin.views._
import resolved.analyses._

/**
 * Tests for `ReportViewContentProvider`.
 *
 * @author Florian Brandherm
 */
@org.junit.runner.RunWith(classOf[org.scalatest.junit.JUnitRunner])
class TestReportViewContentProvider extends PluginTest {

    behavior of "ReportViewContentProvider"

    val contentProvider = new ReportViewContentProvider

    val project1 = createMockJavaProject("project1")
    val project2 = createMockJavaProject("project2")
    val project3 = createMockJavaProject("project3")

    contentProvider.setResults(project1, Array(
        ("a1", Array(
            new ReportData("TestClass", Severity.Warning, "msg1"),
            new ReportData("TestClass", Severity.Warning, "msg2"))),
        ("a3", Nil),
        ("a2", Nil)))

    contentProvider.setResults(project2, Array(
        ("a2", Array(new ReportData("TestClass", Severity.Warning, "msg3"))),
        ("a3", Array(new ReportData("TestClass", Severity.Warning, "msg4")))))

    contentProvider.setResults(project3, Array())

    // Test hasChildren()

    it should "recognize that \"a3\" has no children in project 1." in {
        contentProvider.hasChildren((project1, "a3")) should be(false)
    }

    it should "recognize that \"a1\" has children in project 1." in {
        contentProvider.hasChildren((project1, "a1")) should be(true)
    }

    it should "recognize that \"a2\" has children in project 2." in {
        contentProvider.hasChildren((project2, "a2")) should be(true)
    }

    it should "recognize that \"a2\" has no children in project 1." in {
        contentProvider.hasChildren((project1, "a2")) should be(false)
    }

    it should "recognize that a ReportData object can't have children." in {
        contentProvider.hasChildren((project1,
            new ReportData("TestClass",
                Severity.Warning,
                "msg1"))) should be(false)
    }

    it should "recognize that project1 has children." in {
        contentProvider.hasChildren(project1) should be(true)
    }

    it should "recognize that project3 has no children." in {
        contentProvider.hasChildren(project3) should be(false)
    }

    // Test getChildren()

    it should "get both children of \"a1\" in project 1." in {
        val result = contentProvider.getChildren((project1, "a1"))
        result should equal(
            Array(
                (project1, new ReportData("TestClass", Severity.Warning, "msg1")),
                (project1, new ReportData("TestClass", Severity.Warning, "msg2")))
        )
    }

    it should "return an empty array if getChildren() is called with "+
        "a ReportData Object." in {
            contentProvider.getChildren(
                (project1, new ReportData("TestClass",
                    Severity.Warning, "msg1"))) should be(Nil)
        }

    // Test getParent()

    it should "get the parent object of ReportData(msg3) which is \"a2\"." in {
        contentProvider.getParent((project2,
            new ReportData("TestClass",
                Severity.Warning,
                "msg3"))) should be("a2")
    }

    it should "get a project as parent object for a string" in {
        contentProvider.getParent((project2, "a2")) should be(project2)
    }

    it should "get null as parent for an IJavaProject" in {
        contentProvider.getParent(project3)
    }

    // Test getElements()

    it should "get all the first level nodes (which are the projects) if getElements() "+
        "is called" in {
            val elements = contentProvider.getElements(null)
            elements should contain(project1)
            elements should contain(project2)
            elements should contain(project3)
            elements.length should be(3)
        }
}
